import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the first number: ");
        double num1 = Double.parseDouble(in.next());
        System.out.println("Enter an operator(+,-,*,/): ");
        String operator = in.next();
        System.out.println("Enter the second number: ");
        double num2 = Double.parseDouble(in.next());
        double ans = 0;

        switch (operator){
            case "+":
                ans = num1 + num2;
                break;
            case "-":
                ans = num1 - num2;
                break;
            case "*":
                ans = num1 * num2;
                break;
            case "/":
                ans = num1 / num2;
                break;
            default:
                System.out.println("Wrong operator entered");
        }
        System.out.println("The answer is: " + ans);

    }
}